<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Faker\Factory;

class mainContent{}
class lowerContent{}

Route::get('/', function () {

    $faker = Factory::create();

    $main = new mainContent();
    $main->image = $faker->imageURL();
    $main->title = $faker->text(75);
    $main->content = $faker->text(180);
    $main->source = $faker->text(10);


    $card1 = new lowerContent();
    $card1->image = $faker->imageURL();
    $card1->title = $faker->text(60);
    $card1->source = $faker->text(5);


    $card2 = new lowerContent();
    $card2->image = $faker->imageURL();
    $card2->title = $faker->text(60);
    $card2->source = $faker->text(5);

    $card3 = new lowerContent();
    $card3->image = $faker->imageURL();
    $card3->title = $faker->text(60);
    $card3->source = $faker->text(5);

    $card4 = new lowerContent();
    $card4->image = $faker->imageURL();
    $card4->title = $faker->text(60);
    $card4->source = $faker->text(5);

    $card5 = new lowerContent();
    $card5->image = $faker->imageURL();
    $card5->title = $faker->text(60);
    $card5->source = $faker->text(5);

    $card6 = new lowerContent();
    $card6->image = $faker->imageURL();
    $card6->title = $faker->text(60);
    $card6->source = $faker->text(5);



    $data = [
        'main' => [$main],
        'cards' => [$card1, $card2, $card3, $card4, $card5, $card6],
    ];
    return view('welcome', $data);
});
