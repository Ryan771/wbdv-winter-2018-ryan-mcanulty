@extends('layout')
<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>

        </style>
    </head>
    <body>
@section('content')

        <header class="site-header">
            <a href="" class="logo"><img src="https://i.cbc.ca/1.4066392.1500303304!/fileImage/httpImage/image.jpg_gen/derivatives/16x9_620/cbc-logo-horizontal.jpg"></a>
             <nav class="site-nav">
               <ul>
                 <li class="active"><a href="">Menu</a></li>
                 <li class="fa"><a href="" class="fa"><i class="fal fa-chevron-down"></i></a></li>
               </ul>
             </nav>

             <div class="searchIcon">
                 <a href="" class="search"><i class="fas fa-search"></i></a>
             </div>
         </header>
         <hr>

         <div class="mainContent">
             <div class="homepage">
                 <h3> WELCOME TO CBC.CA</h3>
                 <hr class="homepageHr">


                    <?php foreach($main as $main): ?>
                     <div class ="firstContent">
                         <img class="mainImage" src="<?php echo $main->image ?>"/>
                         <div class="content">
                             <p class="mainSource">Listen</p>
                             <h3 class="mainTitle"><?php echo $main->title?></h3>
                             <p class="mainContent"><?php echo $main->content ?></p>
                             <p class="mainSource"><?php echo $main->source?> - <?php echo $main->source?></p>
                         </div>
                     <?php endforeach ?>
                    </div>
            </div>

            <div class=socialMediaBox>
                <h3>FOLLOW CBC</h3>
                <hr class="homepageHr">
                <div class="socialIcons">
                    <a><i class="fab fa-facebook fa-2x"></i></a>
                    <a><i class="fab fa-twitter fa-2x"></i></a>
                    <a><i class="fab fa-youtube fa-2x"></i></a>
                    <a><i id="insta" class="fab fa-instagram fa-2x"></i></a>
                </div>
            </div>
        </div>

    <br/>

    <br/>

        <div class="lowerContent">
            <div class="container">
                <ul>
                    <div class="lowerContentCard">
                        <?php foreach($cards as $card): ?>
                            <li>
                                <img src="<?php echo $card->image ?>"/>
                                <div class="cardContainer">
                                    <h3><?php echo $card->title ?></h3>
                                    <p class="mainSource"> <?php echo $card->source ?> </p>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </div>
                </ul>
            </div>
        </div>


        


    </body>
</html>
